<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>API-UP-001-Update Profile with valid data</name>
   <tag></tag>
   <elementGuidId>918d2cc3-22cb-4c40-a5d4-bc8ffee2a4bb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiY2U5NzBiMjU3NzMyZjA1ZDRjNzljN2VjM2U2NzFiMzYwMWRjYWJjZWE2NWZiYzZmZDA1OGEzMzQxZWIyZjZjMTAzM2YyNzFiOGFmZDk0OTIiLCJpYXQiOjE2OTgyMTY3OTUuNTY1NTQyLCJuYmYiOjE2OTgyMTY3OTUuNTY1NTQ1LCJleHAiOjE3Mjk4MzkxOTUuNTYyMzg5LCJzdWIiOiIxMTI3Iiwic2NvcGVzIjpbXX0.mjXq10LLmnQ1c8rdxZZMAyYtwYTvSCDPCQ0zm7zB6V3x7Zp2sRVMDBh7BVduks4mQzAE5OoJcw8PrOLVlCM_NbkObRe38kePIYEpXp534y0Fs_GyEQxEM0E2SrYHkbZRVtwSKhWSG5u9SmVxkn2CKnvnXwE4jg4-l1zxNpmeP0C00mDJW09gl24aXtxXOY492BM_cKkNL4Fzw9d8X3JbUNyH9fyqdqmq3dM7r4tPLz75rTqIqzfjmYdyO7_IRZhMmXzG9tFIJ_p29UkoDzJhvngROQ83dt5FtLbuL9ONAM4SWT0BlI5r4jCRX3IUXAZg5x4gf8LQodYHmuiW1kw71BToOzcZA1NQ4SvbHOUATNuq_Zsr0CPC_P0C58W00g8QxGVBljlRv0V_EEa5RRsiHpxbfQISPycuD7HzKBMIh3Sl-ltLtow55HzpGfBqdvqNPjgKK5Ii8Y50inRjeMFC06Kw2shHZDjk0IeyN1tJfWnqF3NVn9V6pp7J4vsb6t66BYaSzQc0j_685TzNhIbpkhLj9XbjEwyDo3JJsKHxUjG-dPCnJljjDHaAMhJsTUMObO9iRdNe_1n2aQXljxC-wnkTiNWEkfBfBn2gHOmQ4k9LbU0RRMPurkocNkD7XX28nlFZIQGRCTkIeLNQQPJUiAsRr7-QvhYRSc-kWutCBbQ</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;Putra&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;0812345678&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;2000-07-18&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;Software Dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;mobile dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>e5ad1416-d3a4-48ed-a20f-ddcca5fd0aa2</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiY2U5NzBiMjU3NzMyZjA1ZDRjNzljN2VjM2U2NzFiMzYwMWRjYWJjZWE2NWZiYzZmZDA1OGEzMzQxZWIyZjZjMTAzM2YyNzFiOGFmZDk0OTIiLCJpYXQiOjE2OTgyMTY3OTUuNTY1NTQyLCJuYmYiOjE2OTgyMTY3OTUuNTY1NTQ1LCJleHAiOjE3Mjk4MzkxOTUuNTYyMzg5LCJzdWIiOiIxMTI3Iiwic2NvcGVzIjpbXX0.mjXq10LLmnQ1c8rdxZZMAyYtwYTvSCDPCQ0zm7zB6V3x7Zp2sRVMDBh7BVduks4mQzAE5OoJcw8PrOLVlCM_NbkObRe38kePIYEpXp534y0Fs_GyEQxEM0E2SrYHkbZRVtwSKhWSG5u9SmVxkn2CKnvnXwE4jg4-l1zxNpmeP0C00mDJW09gl24aXtxXOY492BM_cKkNL4Fzw9d8X3JbUNyH9fyqdqmq3dM7r4tPLz75rTqIqzfjmYdyO7_IRZhMmXzG9tFIJ_p29UkoDzJhvngROQ83dt5FtLbuL9ONAM4SWT0BlI5r4jCRX3IUXAZg5x4gf8LQodYHmuiW1kw71BToOzcZA1NQ4SvbHOUATNuq_Zsr0CPC_P0C58W00g8QxGVBljlRv0V_EEa5RRsiHpxbfQISPycuD7HzKBMIh3Sl-ltLtow55HzpGfBqdvqNPjgKK5Ii8Y50inRjeMFC06Kw2shHZDjk0IeyN1tJfWnqF3NVn9V6pp7J4vsb6t66BYaSzQc0j_685TzNhIbpkhLj9XbjEwyDo3JJsKHxUjG-dPCnJljjDHaAMhJsTUMObO9iRdNe_1n2aQXljxC-wnkTiNWEkfBfBn2gHOmQ4k9LbU0RRMPurkocNkD7XX28nlFZIQGRCTkIeLNQQPJUiAsRr7-QvhYRSc-kWutCBbQ</value>
      <webElementGuid>d46c3559-4e70-4b1b-afd0-af6cf422a33d</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.site/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
