<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Btn_Daftar Sekarang_QA</name>
   <tag></tag>
   <elementGuidId>2fdd4f93-6ec4-4512-9057-19dc00ee4e8c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.hoveredContainer</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>0f0f479c-f499-4512-878d-768c16ed6002</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>hoveredContainer</value>
      <webElementGuid>6eb0800d-2dcc-47e1-b21e-f701f407908e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                Daftar Sekarang
                            
                        </value>
      <webElementGuid>1c768161-39cb-4385-ba20-2dfb704acb27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/div[@class=&quot;containerRow&quot;]/div[@class=&quot;hoveredContainer&quot;]</value>
      <webElementGuid>c4719bb0-d457-4203-b040-79e32ed0e437</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/div/div[2]</value>
      <webElementGuid>34e63e32-18a1-48c8-bf45-80d8a99afdaa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/following::div[1]</value>
      <webElementGuid>4d14c724-0a30-4e42-92af-f889787e768e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/following::div[1]</value>
      <webElementGuid>a88d7fee-5c74-4aa1-bb88-1f42975b74d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Fullstack Engineer Class'])[2]/preceding::div[1]</value>
      <webElementGuid>f84542e3-ccf8-47b9-b3ff-cf683ce9b96e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]</value>
      <webElementGuid>a67cb0c6-fadd-4733-b6b4-9343428fd74b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                                Daftar Sekarang
                            
                        ' or . = '
                            
                                Daftar Sekarang
                            
                        ')]</value>
      <webElementGuid>29daa5b9-731d-4584-805d-ef866357b094</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
