<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h6_Day 3 Predict using Machine Learning</name>
   <tag></tag>
   <elementGuidId>14d5f8ad-b22f-4131-a67b-80d6acac902e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]/h6[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>02ae1272-f316-4bce-bc31-cbdba5694379</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_6</value>
      <webElementGuid>e20074e0-caed-48f8-bdc0-cd33fb8b970b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Day 3: Predict using Machine Learning</value>
      <webElementGuid>0fa9af67-95d6-4831-a8c7-659af371da69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]/div[@class=&quot;containerCardBootcamp&quot;]/h6[@class=&quot;new_headline_6&quot;]</value>
      <webElementGuid>8ca0b598-a716-40c1-b844-c951c67926be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]/h6[2]</value>
      <webElementGuid>fc6bc5df-22e1-4e10-8e41-2069aaee311d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVENT'])[1]/following::h6[1]</value>
      <webElementGuid>6fecfc61-fd8a-463b-94dc-08e13cb61566</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::h6[2]</value>
      <webElementGuid>05aaa1cf-4e29-49e2-80ab-145730c8ee87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[2]/preceding::h6[1]</value>
      <webElementGuid>55d17341-498f-4336-a524-6e7beec6e5de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[2]/preceding::h6[1]</value>
      <webElementGuid>2b6ca0d6-2738-47f7-9590-4798964ad41e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 3: Predict using Machine Learning']/parent::*</value>
      <webElementGuid>ec614205-eb58-48e4-910f-a0ea67d86fff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/h6[2]</value>
      <webElementGuid>2bb588df-3e07-4306-b426-7b2af824c9b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                Day 3: Predict using Machine Learning' or . = '
                                Day 3: Predict using Machine Learning')]</value>
      <webElementGuid>66c64016-2c80-42a1-b982-95dd01e9bf47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
