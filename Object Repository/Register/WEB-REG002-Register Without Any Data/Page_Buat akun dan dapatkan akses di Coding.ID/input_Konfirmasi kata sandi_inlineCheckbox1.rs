<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_inlineCheckbox1</name>
   <tag></tag>
   <elementGuidId>90517208-998d-4766-8c30-541585f55529</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='inlineCheckbox1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#inlineCheckbox1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e80bb9e6-3af3-426f-a489-a0626cca91ff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input</value>
      <webElementGuid>64852775-6fad-49e4-a837-a62dcc0e842a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>22402b0a-eb24-41a4-986e-5421cd3ab67f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>inlineCheckbox1</value>
      <webElementGuid>9c056816-a540-475f-9a83-92fcaaf9f7b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>option1</value>
      <webElementGuid>35a38470-c0be-4086-a42c-18dcc310979e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inlineCheckbox1&quot;)</value>
      <webElementGuid>9d84029f-734b-4872-80b7-7b7fb8122168</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='inlineCheckbox1']</value>
      <webElementGuid>969463a6-8576-4239-bfce-b38bb730dfec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/input</value>
      <webElementGuid>025ea051-e51c-4e6e-be0b-0e9f640a5e3b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'inlineCheckbox1']</value>
      <webElementGuid>a7985553-f208-47b3-9671-d456dc0b7c92</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
