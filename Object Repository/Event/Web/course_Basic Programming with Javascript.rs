<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>course_Basic Programming with Javascript</name>
   <tag></tag>
   <elementGuidId>a35f10ae-aafb-48a6-8185-7f228a44e696</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.titleCourse</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='containerCard']/div/a/div[2]/h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>b97b8a8b-37c1-433c-9443-0db928241ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>titleCourse</value>
      <webElementGuid>2e8f0dfc-4cee-4b67-832d-7af36e280634</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Basic Programming with Javascript </value>
      <webElementGuid>c380c27a-7bd8-47b1-995f-2efbcedf21af</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;containerCard&quot;]/div[@class=&quot;cardOuter&quot;]/a[1]/div[@class=&quot;cardBody&quot;]/h3[@class=&quot;titleCourse&quot;]</value>
      <webElementGuid>4d0882c2-57a0-4215-a055-b594827059ba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='containerCard']/div/a/div[2]/h3</value>
      <webElementGuid>ed83e1c6-7260-4f88-8140-177c753a98e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas Terbaru'])[1]/following::h3[2]</value>
      <webElementGuid>7020fd9c-1151-48ec-bcbb-97da4b213eb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Online Courses'])[1]/following::h3[2]</value>
      <webElementGuid>b9a9bce2-2703-4b10-b28b-e48b769e7721</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Zakka,'])[1]/preceding::h3[1]</value>
      <webElementGuid>0fe00aed-d70a-4080-b385-0b7bbfff20cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.500.000'])[1]/preceding::h3[1]</value>
      <webElementGuid>c7aabb88-0a95-4870-b00d-ae6efb5e7045</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Basic Programming with Javascript']/parent::*</value>
      <webElementGuid>4a8d4303-cd46-4fb5-b186-d5d61afd09e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/h3</value>
      <webElementGuid>e0120037-972d-471c-b3e1-bd8a7f88c357</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Basic Programming with Javascript ' or . = 'Basic Programming with Javascript ')]</value>
      <webElementGuid>841ce930-0bcd-42ba-be24-0351b3530e25</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
