<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Kata                                 _98da12</name>
   <tag></tag>
   <elementGuidId>de2c219d-b95b-4263-8db2-505bdb77861b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#password</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>df43e9f2-bfa5-450e-a9e4-9e1b1b075c72</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>ba70a7e5-9b4f-40c2-95e3-baee0d095b4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>a3de5c70-5ef2-4f1b-9f2c-b4ccdc3c8e4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Kata Sandi</value>
      <webElementGuid>abce5226-19a6-470d-80b2-8aa02ed97dfa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>487472ba-fdb6-4f04-a318-761b578fc92a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>275f06e1-7a8e-4c86-bd4b-a3c1373cc9f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>2</value>
      <webElementGuid>579319b0-5448-4c2e-8f36-4fa3c5ae477d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;password&quot;)</value>
      <webElementGuid>fe7d28fa-8612-4f59-8f53-645a7cec5dfb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='password']</value>
      <webElementGuid>e0a17a6b-a59a-4fc3-8124-534340c6334b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>dca8654f-6650-4e88-bb2a-85506bcaf4f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'password' and @type = 'password' and @placeholder = 'Kata Sandi' and @name = 'password']</value>
      <webElementGuid>efea9fbf-e895-4e8b-b401-95c54bef40e3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
